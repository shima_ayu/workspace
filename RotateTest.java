import static org.junit.Assert.*;

import org.junit.Test;


public class RotateTest {
	@Test
	public void test() {
		int[] a = {1,2,3,4,5};
		assertArrayEquals(new int[]{3,4,5,1,2},Rotate.rotate(a, -2));
	}
	@Test
	public void test2() {
		int[] a = {1,2,3,4,5};
		assertArrayEquals(new int[]{4,5,1,2,3},Rotate.rotate(a, -2));
	}
}
Shima Ayu

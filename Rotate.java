import java.util.Scanner;

public class Rotate {  	
	public static int k;
	public static void main(String[] args) {
    	Scanner stdIn = new Scanner(System.in);	
    	int[] a = {1, 2, 3, 4, 5};
    	int[] b = new int[a.length];
  
    	
        	// 表示する。
        System.out.print("a = ");
        print(a);
        System.out.print("please input k=");
        k = stdIn.nextInt();

        // ずらしてコピーする。
        b = rotate(a,k);

        // 表示する。
        System.out.print("b = ");
        print(b);
    }

    /**
     * 指定された配列aの各要素を1つずらして、新しい配列を作って返す。
     * @param a 配列
     * @return 要素を1つずらした配列
     */
   public static int[] rotate(int[] a,int k) {
    	int i,j;
        int[] b = new int[a.length];
        int[] tmp=new int[a.length];
        if(k<0){
        	while(k<0){
        		k+=a.length;
        	}
        }
        	
        for ( i = 0; i < a.length  ;i++){
        	j=i-k;
        	if(j<0){
        		j+=a.length;
        	}
        	tmp[i]=a[j];
        }
        	//b[(i + k) % b.length] = a[i];
        for(i=0;i<a.length;i++){
        	b[i]=tmp[i];
        }
        return b;
    }

    /**
     * 指定された配列aの各要素を並べて表示する。
     * @param a 配列
     */
    public static void print(int[] a) {
        System.out.print("[");
        for (int i = 0; i < a.length -1 ; i++){
            System.out.print(a[i] + ", ");
        }
        System.out.println(a[a.length-1] + "]");
        //System.out.println("]");
    }
}Shima Ayu
